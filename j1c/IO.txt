I/O Memory ports

Misc.

    0x6200    J1HALT
              Writing to this location will halt the simulation.

Console IO:

    0x5000  RS232_TXD
    0x5002  RS232_RXD

      Writing data to RS232_TXD sends the data to console.
      Reading data from RS232_RXD gets keypress data from the console.
      If the sign bit is set, the keypress data is in the low eight bits.
      If the sign bit is clear, no keypress data.

Hardware multiplier

    0x6100 MULT_A 
    0x6102 MULT_B 
    0x6104 MULT_P 

      Unsigned 16 bit by 16 bit multiply yielding a 32 bit result.

      1. Write first operand to MULT_A.
      2. Write second operand to MULT_B; this starts the multiply.
      3. Read high 16 bits of result from MULT_P.
      4. Read low 15 bits or result from MULT_P + 2.

          : um*   mult_a ! mult_b ! mult_p 2@ ;

DN355 Memory

    32,768 words of 18 bit memory. Addresses are 15 bits.

    0x4000 DN355_MEMORY_ADDR
    0x4004 DN355_MEMORY_DATA_HIGH
    0x4006 DN355_MEMORY_DATA_LOW
    0x4008 DN355_MEMORY_CTRL

    To read from memory:

       Write the address in DN355_MEMORY_ADDR.
       Write 0x0001 to DN355_MEMORY_CTRL.
       Wait for memory cycle to complete.
       Read the high 2 bits of the data from DN355_MEMORY_DATA_HIGH.
       Read the low 16 bits of the data from DN355_MEMORY_DATA_LOW.

            : mem_read ( address -- dataLow dataHigh )
                DN355_MEMORY_ADDR !
                DN355_MEMORY_CTRL_RD DN355_MEMORY_CTRL !
                noop noop noop
                DN355_MEMORY_DATA 2@
            ;

    To write to memory:

       Write the address in DN355_MEMORY_ADDR.
       Write the high 2 bits of the data to DN355_MEMORY_DATA_HIGH.
       Write the low 16 bits of the data to DN355_MEMORY_DATA_LOW.
       Write 0x0002 to DN355_MEMORY_CTRL.

            : mem_write ( address dataLow dataHigh )
                DN355_MEMORY_ADDR !
                DN355_MEMORY_DATA 2!
                DN355_MEMORY_CTRL_WR DN355_MEMORY_CTRL !
                noop noop noop
            ;

