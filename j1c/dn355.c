#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "j1c.h"
#include "dn355.h"

typedef uint8_t  word2;
typedef uint16_t word15;
typedef uint32_t word18;


// Memory size
enum { MEM_SIZE = 1 << 15 };

static word18 M[MEM_SIZE];
static word15 M_address;
static word2  M_dataHigh;
static word18 M_data;

#define ASSEMBLE(h,l) ((((word18) ((h) & 0x3)) << 16) | (l))

bool dn355_io_din (cell address, cell * data) {
  if (address == DN355_MEMORY_DATA_HIGH) {
    * data = (M_data >> 16) & 3;
//printf ("rh %0o\n", * data);
    return true;
  }
  if (address == DN355_MEMORY_DATA_LOW) {
    * data = M_data & 0xffff;
//printf ("rl %0o\n", * data);
    return true;
  }
  return false;
}

bool dn355_io_dout (cell address, cell data) {
  if (address == DN355_MEMORY_ADDR) {
    M_address = data & 0x7fff;
//printf ("ad %05o\n", M_address);
    return true;
  }
  if (address == DN355_MEMORY_DATA_HIGH) {
    M_dataHigh = data & 0x3;
//printf ("dh %o\n", M_dataHigh);
    return true;
  }
  if (address == DN355_MEMORY_DATA_LOW) {
    M_data = ASSEMBLE (M_dataHigh, data);
//printf ("dl %06o %06o %06o\n", M_dataHigh, data, M_data);
    return true;
  }
  if (address == DN355_MEMORY_CTRL) {
    if (data == DN355_MEMORY_CTRL_RD) {
      M_data = M[M_address];
//printf ("rd %05o:%06o\n", M_address, M_data);
      return true;
    }
    if (data == DN355_MEMORY_CTRL_WR) {
      M[M_address] = M_data;
//printf ("wr %05o:%06o\n", M_address, M_data);
      return true;
    }
    fprintf (stderr, "bad memory ctrl %04x\n", data);
  }
  return false;
}
