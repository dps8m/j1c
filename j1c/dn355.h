// Memory unit address buffer
#define DN355_MEMORY_ADDR      0x4000 // word
// Memory unit data buffer
#define DN355_MEMORY_DATA      0x4004 // dword
#define DN355_MEMORY_DATA_HIGH 0x4004 // word
#define DN355_MEMORY_DATA_LOW  0x4006 // word
// Memory unit control
#define DN355_MEMORY_CTRL      0x4008 // word
#define DN355_MEMORY_CTRL_RD        0x0001  // Write to CTRL to load memory at ADDRESS into DATA
#define DN355_MEMORY_CTRL_WR        0x0002  // Write to CTRL to write DATA to memory at ADDRESS
#define DN355_MEMORY_CTRL_RD_DELAY    3  // Number of J1 cycles to complete CTRL_RD
#define DN355_MEMORY_CTRL_RD_DELAY_NOOP noop noop noop
#define DN355_MEMORY_CTRL_WR_DELAY    5  // Number of J1 cycles to complete CTRL_RD
#define DN355_MEMORY_CTRL_WR_DELAY_NOOP noop noop noop noop noop

bool dn355_io_din (cell address, cell * data);
bool dn355_io_dout (cell address, cell data);

