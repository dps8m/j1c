all : FIRMWARE J1C

.PHONY: FIRMWARE J1C

FIRMWARE :
	cd firmware && make

J1C :
	cd j1c && make
