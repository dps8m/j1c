h# 4000 constant DN355_MEMORY_ADDR \ word
h# 4004 constant DN355_MEMORY_DATA \ dword
h# 4008 constant DN355_MEMORY_CTRL \ word
h# 0001 constant DN355_MEMORY_CTRL_RD \ data
h# 0002 constant DN355_MEMORY_CTRL_WR \ data
d# 3    constant DN355_MEMORY_CTRL_RD_DELAY \ cycles
\ : DN355_MEMORY_CTRL_RD_DELAY_NOOP noop noop noop ;
d# 5    constant DN355_MEMORY_CTRL_WR_DELAY
\ : DN355_MEMORY_CTRL_WR_DELAY_NOOP noop noop noop noop noop ;

h# 6200 constant j1halt

: .18 ( dataLow dataHigh )
   swap  ( hi lo )
   d# 8 base !
   <# # # # # # # #> type
;


: mem_read ( address -- dataLow dataHigh )
    DN355_MEMORY_ADDR !
    DN355_MEMORY_CTRL_RD DN355_MEMORY_CTRL !
    noop noop noop
    DN355_MEMORY_DATA 2@
;

: mem_write ( address dataLow dataHigh )
    DN355_MEMORY_ADDR !
    DN355_MEMORY_DATA 2!
    DN355_MEMORY_CTRL_WR DN355_MEMORY_CTRL !
    noop noop noop
;

\ In each word, write the low 3 bits of the address in the high 3 bits and
\ the address in the low 15 bits; c take the oone's complement of the low 16
\  bits. Read them back and verify contents

variable mem_test_cr 
: 1cr mem_test_cr @ 0= if cr d# 1 mem_test_cr ! then ;

: mem_test
    d# 0 mem_test_cr !
    h# 8000 0do
      i d# 1 rshift d# 3 and \ bits 13 & 14 of the address in the high 2 bits of the date
      i d# 1 and d# 15 lshift i or invert \ bit 15 of the address in the 3rd data bit, address in the low 15 bits; invert.
      i mem_write
    loop
    h# 8000 0do
      i mem_read 
        i d# 1 and d# 15 lshift i or invert = 0= if 1cr s" errorl " type i . i mem_read .18 cr then
        i d# 1 rshift d# 3 and = 0= if 1cr s" errorh " type i . i mem_read .18 cr then
    loop
;

: dbg_help
  cr
  s" (m)emory (q)uit" type cr
;

: dbg_getkey ( -- key )
  begin
    \ RS232_RXD @ ?dup if h# ff and d# 1 else 0 then
    RS232_RXD @ dup ( key key )
    if ( key ) h# ff and d# 1 ( key 1 ) 
    else drop d# 0 ( 0 ) 
    then
  until
;

: dbg_prompt cr s" ?" type ;

variable dbg_run
variable dbg_addr
variable dbg_in_number

: o. d# 8 base ! s>d <# # # # # # #> type ;

: dbg_mem
  cr
  s" address:" type
  d# 0 dbg_addr !
  begin
    d# 0  ( flag ) \ stay in loop
    dbg_getkey ( flag key )
    dup [char] 0 < 0= over [char] 8 < and if 
      dup emit
      [char] 0 - dbg_addr @ d# 3 lshift or dbg_addr !
    else dup d# 10 ( nl ) = if ( flag key )
      drop 
      cr dbg_addr @ o. [char] : emit
      dbg_addr @ mem_read .18 
      dbg_addr @ d# 1 + dbg_addr ! 
      \ dbg_addr @ o.
    else dup [char] q = if
      drop drop d# 1 
    else
      drop
    then ( q ) then ( nl ) then ( digit)
  until
  cr
;

\        drop drop d# 1 ( flag ) \ exit loop
\      else dup d# 10 ( nl ) = if ( flag key )
\        dbg_addr @ d# 1 + dup dbg_addr ! o. [char] : emit dbg_addr @ mem_read .18
\      else
\        drop
\      then ( cr ) then ( q )
\    until
  
   \ dbg_addr @ dup o. [char] : emit mem_read .18 
0 [IF]
    begin
      d# 0  ( flag ) \ stay in loop
      dbg_getkey ( flag key )
      dup [char] q = if ( flag key )
        drop drop d# 1 ( flag ) \ exit loop
      else dup d# 10 ( nl ) = if ( flag key )
        cr
        dbg_addr @ o. [char] : emit dbg_addr @ mem_read .18
        dbg_addr @ d# 1 + dbg_addr ! 
        \ cr dbg_addr @ o. [char] : emit 
      else
        drop
      then ( cr ) then ( q )
    until
[ENDIF]

: dbg
    s" debugger " type cr
    dbg_help  
    begin
      dbg_prompt
      d# 0 ( flag )
      dbg_getkey ( flag key )
      dup [char] q = if ( flag key )
        cr
        swap drop d# 1 swap ( flag key )
      else dup [char] m = if ( flag key )
        cr dbg_mem
      else
        cr s" ???" type cr
        dbg_help
      then ( m ) then ( q )
      drop ( key )
    until

0 [IF]
    s" debugger " type cr
    begin
      dbg_getkey 
    d# 1 dbg_run !
    d# 0 dbg_addr !
    d# 0 dbg_in_number !
    dbg_help  
    dbg_prompt
    \ command loop
    begin
      dbg_getkey 
      dup [char] q = if 
        d# 0 dbg_run !
      else dup [char] m = if 
        dbg_m
        d# 0 dbg_addr !
        d# 0 dbg_in_number !
      else dup [char] 0 < 0= over [char] 8 < and if 
        dup [char] 0 - dbg_addr @ d# 3 lshift or dbg_addr !
        d# 1 dbg_in_number !
      else
        cr s" ? " type dbg_help dbg_prompt
      then ( digit )  then ( m ) then ( q )
      drop
      dbg_in_number @ 0= if dbg_prompt then
    dbg_run @ 0= until
[ENDIF]
;

: dn355

    d# 6 0do cr loop
    s" DN355 simulator " type cr

    s" Memory test .... " type 
    mem_test
    s" done " type cr

   dbg
;
